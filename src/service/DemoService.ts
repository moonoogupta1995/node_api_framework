import { injectable, inject } from 'inversify'

import { Demo } from '../model/Demo';


import { DemoRepository } from "../repository/DemoRepository"

import TYPES from '../types/types'

import { DemoDTO } from '../schema/DemoSchema'
import { promises } from 'dns';



export interface DemoService {
    getDemo()
    createDemo(demoData)
    deleteDemo(id)
}


@injectable()
export class DemoServiceImpl implements DemoService {

    
    @inject(TYPES.DemoNoSQLRepository)
    private DemoRepositoryMongo: DemoRepository

   
    public async getDemo() {
        const demosMongo = await this.DemoRepositoryMongo.findAll()
        return demosMongo;
    };



    public async  createDemo(demoData) {
        var exist = await this.DemoRepositoryMongo.getDemo(demoData.email)
        if (exist) {
            var createdDTO = await this.DemoRepositoryMongo.createDemo(demoData);
        }
        else {
            return Promise.reject("email Id already exist")
        }
        return createdDTO
    }

   
    public async deleteDemo(demoId) {
        return await this.DemoRepositoryMongo.deleteDemo(demoId)
    }




}
