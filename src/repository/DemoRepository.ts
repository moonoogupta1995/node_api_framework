import { injectable } from 'inversify'
import { Repository, Connection } from 'typeorm';
import { DemoDTO, DemosMongoDatabase, DemoMongoSchema } from '../schema/DemoSchema';

const CONNECTION_NAME = 'demo'


export interface DemoRepository {
    findAll()
    createDemo(demoData)
    getDemo(id)
    deleteDemo(id)
}




@injectable()
export class DemoRepositoryMongo implements DemoRepository {

    public async findAll() {
        const DemoDTOs = await DemosMongoDatabase.connect().then(() => DemosMongoDatabase.model.find())
        return DemoDTOs.toArray()
    }

    
    public async createDemo(demoData) {
        return await DemosMongoDatabase.connect().then(() => {
            return DemosMongoDatabase.model.insert(demoData)
        })

    }


    getDemo(id) {
        return DemosMongoDatabase.connect().then(() => {
            return DemosMongoDatabase.model.findOne({ email: id })
        })
    }
  
    public async  deleteDemo(id) {
        return await DemosMongoDatabase.connect().then(() => {
            return DemosMongoDatabase.model.remove({ _id: id })
        })
    }



}
