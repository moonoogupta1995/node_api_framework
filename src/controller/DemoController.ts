import { BaseController } from './BaseController'
import { injectable, inject } from 'inversify';
import * as express from 'express'
import colors = require('colors')
import { interfaces, controller, httpGet, httpPost, httpDelete, httpPut } from 'inversify-express-utils';
import { DemoService } from '../service/DemoService';

import { DemoDTO, DemosMongoDatabase, } from '../schema/DemoSchema';
import { Demo } from '../model/Demo';

import TYPES from '../types/types'




@controller('/demo')
@injectable()
export class DemoController extends BaseController implements interfaces.Controller {

    private demoService: DemoService

    constructor(@inject(TYPES.DemoService) demoService: DemoService) {
        super()
        this.demoService = demoService

    }

 
    @httpGet('/')
    private async getDemo(req: express.Request, res: express.Response, next: express.NextFunction) {
        const demo = await this.demoService.getDemo().catch(err => console.log(err))
        return this.renderJSON(req, res, { demo: demo })
    }

    @httpPost('/')
    public async createDemo(req: express.Request, res: express.Response, next: express.NextFunction) {
        const demoData = req.body

        try {
            var createdDemo = await this.demoService.createDemo(demoData)
        } catch (error) {
            return this.renderError(req, res, error)
        }
        return this.renderJSON(req, res, { demo: createdDemo }, 201)
    }

 
    @httpDelete('/:demoId')
    private async demoDelete(req: express.Request, res: express.Response, next: express.NextFunction) {
        var demoid = req.params.demoId;
        const deletedDemo = await this.demoService.deleteDemo(demoid).catch(err => console.log(err))
        return this.renderJSON(req, res, { places: deletedDemo }, 204)
    }

}
