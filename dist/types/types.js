"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TYPES = {
    DemoService: Symbol('DemoService'),
    DemoNoSQLRepository: Symbol('DemoNoSQLRepository'),
};
exports.default = TYPES;
//# sourceMappingURL=types.js.map